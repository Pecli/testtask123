﻿using UnityEngine;
using Enums;
using System.Collections.Generic;
using TMPro;

public class Guild : MonoBehaviour
{
    public Transform Table;
    public GameObject TextPrefab;
    private List<GameObject> _displayedTexts = new List<GameObject>();

    private List<Trader> _traders = new List<Trader>();
    private int _currentYear;
    private int _traderCount;

    private void Start()
    {
        FillGuild();
    }

    public void CalculateYear()
    {
        BeingYear();
        for (int i = 0; i < 60; i++)
        {
            for (int j = 0; j < 60; j++)
            {
                if (i != j)
                {
                    MakeDeals(_traders[i], _traders[j]);
                }
            }
        }
        SortTraders();
        DisplayTraders();
        ReplaceWorstTraders();
        _currentYear++;
    }

    private void DisplayTraders()
    {
        foreach(GameObject textPanel in _displayedTexts)
        {
            Destroy(textPanel);
        }
        _displayedTexts.Clear();
        for (int i = 0; i < 60; i++)
        {
            _displayedTexts.Add(Instantiate(TextPrefab, Table));
            _displayedTexts[i].transform.SetAsFirstSibling();
            _displayedTexts[i].GetComponent<TextMeshProUGUI>().text = _traders[i].Name + " " + _traders[i].GetCoins().ToString();
        }
    }

    private void BeingYear()
    {
        foreach (Trader trader in _traders)
        {
            trader.SetCoins(0);
        }
    }

    private void ReplaceWorstTraders()
    {
        for(int i =0; i < 12; i++)
        {
            _traders[i] = new Trader();
            _traders[i].Type = _traders[59 - i].Type;
            NameTrader(i);
        }
    }

    private void SortTraders()
    {
        Trader tmp;
        for(int i =0; i < _traders.Count; i++)
        {
            for(int j=i+1; j <_traders.Count; j++)
            {
                if (_traders[i].GetCoins() > _traders[j].GetCoins())
                {
                    tmp = _traders[i];
                    _traders[i] = _traders[j];
                    _traders[j] = tmp;
                }
            }
        }
    }

    private void MakeDeals(Trader trader1, Trader trader2)
    {
        int dealsCount = Random.Range(5, 10);
        if(trader1.Type != ETraderType.Cunning || trader2.Type != ETraderType.Cunning || _currentYear < 1)
            for (int i = 0; i < dealsCount; i++)
            {
                if (Random.Range(0f, 1f) < trader1.ChanceToCheat)
                {
                    if (Random.Range(0f, 1f) < trader2.ChanceToCheat)
                    {
                        trader1.DoubleCheated();
                        trader2.DoubleCheated();
                    }
                    else
                    {
                        trader1.Cheated();
                        trader2.GotCheatedOn();
                    }
                }
                else
                {
                    if (Random.Range(0f, 1f) < trader2.ChanceToCheat)
                    {
                        trader1.GotCheatedOn();
                        trader2.Cheated();
                    }
                    else
                    {
                        trader1.MadeFairDeal();
                        trader2.MadeFairDeal();
                    }
                }
            }
        else if(trader1.Type == ETraderType.Cunning)
        {
            for (int i = 0; i < dealsCount; i++)
            {
                if (Random.Range(0f, 1f) < trader2.ChanceToCheat)
                {
                    trader1.DoubleCheated();
                    trader2.DoubleCheated();
                }
                else
                {
                    trader1.MadeFairDeal();
                    trader2.MadeFairDeal();
                }
            }
        }
        else if (trader2.Type == ETraderType.Cunning)
        {
            for (int i = 0; i < dealsCount; i++)
            {
                if (Random.Range(0f, 1f) < trader1.ChanceToCheat)
                {
                    trader1.DoubleCheated();
                    trader2.DoubleCheated();
                }
                else
                {
                    trader1.MadeFairDeal();
                    trader2.MadeFairDeal();
                }
            }
        }
        else if(trader2.Type == ETraderType.Cunning && trader1.Type == ETraderType.Cunning)
        {
            for (int i = 0; i < dealsCount; i++)
            {
                if (Random.Range(0f, 1f) < 0.5f)
                {
                    trader1.DoubleCheated();
                    trader2.DoubleCheated();
                }
                else
                {
                    trader1.MadeFairDeal();
                    trader2.MadeFairDeal();
                }
            }
        }

    }

    private void FillGuild()
    {
        for (int i = 0; i < 60; i++)
        {
            _traders.Add(new Trader());
            if (i < 10)
            {
                _traders[i].Type = ETraderType.Altruist;
                NameTrader(i);
                _traders[i].ChanceToCheat = 0f;
            }
            else if (i >= 10 && i < 20)
            {
                _traders[i].Type = ETraderType.Cheater;
                NameTrader(i);
                _traders[i].ChanceToCheat = 1f;
            }
            else if (i >= 20 && i < 30)
            {
                _traders[i].Type = ETraderType.Cunning;
                NameTrader(i);
                _traders[i].ChanceToCheat = 0f;
            }
            else if (i >= 30 && i < 40)
            {
                _traders[i].Type = ETraderType.Unpredictable;
                NameTrader(i);
                _traders[i].ChanceToCheat = 0.5f;
            }
            else if (i >= 40 && i < 50)
            {
                _traders[i].Type = ETraderType.Vindictive;
                NameTrader(i);
                _traders[i].ChanceToCheat = 0;
            }
            else
            {
                _traders[i].Type = ETraderType.Quirky;
                NameTrader(i);
                _traders[i].ChanceToCheat = 0;
            }
        }
    }

    private void NameTrader(int i)
    {
        _traders[i].Name = _traders[i].Type.ToString() + _traderCount.ToString();
        _traderCount++;
    }
}
