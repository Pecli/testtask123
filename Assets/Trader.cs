﻿using Enums;
using UnityEngine;

public class Trader : MonoBehaviour
{
    public string Name;
    private int _coins;
    public ETraderType Type;
    public float ChanceToCheat;

    private int _currentRound;
    private bool _wasCheatedOnAsQuirky = false;
    public void Cheated()
    {
        _coins += 5;
        _currentRound++;
        QuirkyBehaviourAdjust();
    }

    public void GotCheatedOn()
    {
        _coins += 1;
        if(Type == ETraderType.Vindictive)
            ChanceToCheat = 1;
        if (Type == ETraderType.Quirky)
            _wasCheatedOnAsQuirky = true;
            _currentRound++;
        QuirkyBehaviourAdjust();
    }

    public void MadeFairDeal()
    {
        _coins += 4;
        _currentRound++;
        QuirkyBehaviourAdjust();
    }

    public void DoubleCheated()
    {
        _coins += 2;
        if (Type == ETraderType.Quirky)
            _wasCheatedOnAsQuirky = true;
        _currentRound++;
        QuirkyBehaviourAdjust();
    }

    public int GetCoins()
    {
        return _coins;
    }
    public void SetCoins(int Coins)
    {
        _coins = Coins;
    }

    private void QuirkyBehaviourAdjust()
    {
        if(Type == ETraderType.Quirky)
        {
            if (_currentRound == 1)
                ChanceToCheat = 1;
            else if (_currentRound == 2 || _currentRound == 3)
                ChanceToCheat = 0;
            else if (_currentRound == 4)
                if (_wasCheatedOnAsQuirky == true)
                    Type = ETraderType.Cheater;
                else
                    Type = ETraderType.Cunning;
        }
    }
}
