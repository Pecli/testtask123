﻿namespace Enums
{
    public enum ETraderType
    {
        Altruist,
        Cheater,
        Cunning,
        Unpredictable,
        Vindictive,
        Quirky
    }
}
